﻿
Shader "Volume/Standard"
{
	Properties{
		_MainTex("Texture", 3D) = "" {}

		[Header(Channels)]
		[Toggle] _Red("Red", Range(0,1)) = 1
		[Toggle] _Green("Green", Range(0,1)) = 1
		[Toggle] _Blue("Blue", Range(0,1)) = 1
		[Toggle] _Alpha("Alpha", Range(0,1)) = 1
	}
	
	SubShader{

		Pass{

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma exclude_renderers flash gles

			#include "UnityCG.cginc"

			float _Red;
			float _Green;
			float _Blue;
			float _Alpha;

			struct vs_input {
				float4 vertex : POSITION;
			};

			struct ps_input {
				float4 pos : SV_POSITION;
				float3 uv : TEXCOORD0;
			};


			ps_input vert(vs_input v)
			{
				ps_input o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = v.vertex.xyz;
				return o;
			}

			sampler3D _MainTex;

			float4 frag(ps_input i) : COLOR
			{
				return (tex3D(_MainTex, i.uv) * fixed4(_Red, _Green, _Blue, _Alpha));
			}

			ENDCG

		}
	}
		Fallback "VertexLit"
}