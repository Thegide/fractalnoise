﻿using UnityEditor;
using UnityEngine;

public class CloudNoiseGenerator : MonoBehaviour {

    public Material material;
    private Texture3D ShapeTex;
    private Texture3D DetailTex;


    private Texture3D tex;
    public int shapeTexSize = 128;
    public int detailTexSize = 32;

    public Perlin perlin;
    public Worley worley;
   
    #region Main methods
    void Start()
    {
        GenerateShapeTexture(shapeTexSize);
        GenerateDetailTexture(detailTexSize);

        material.SetTexture("_MainTex", ShapeTex);
        //material.SetTexture("_Volume", DetailTex);
    }

    void Update()
    {
        
    }

    public void GenerateShapeTexture(int size)
    {
        ShapeTex = new Texture3D(size, size, size, TextureFormat.RGBA32, true);

        Color[] alphaArray = new Color[size * size * size];

        Perlin p = new Perlin(size, size, size);
        Worley w = new Worley(size, size, size);

        Debug.Log("Generating Red Channel...");
        float[] r = p.Sample(3, 6);
        Debug.Log("Generating Green Channel...");
        float[] g = w.Sample(2, 4, true);
        Debug.Log("Generating Blue Channel...");
        float[] b = w.Sample(4, 4, true);
        Debug.Log("Generating Alpha Channel...");
        float[] a = w.Sample(6, 4, true);

        int i = 0;
        for (int z = 0; z < size; z++)
        {
            for (int y = 0; y < size; y++)
            {
                for (int x = 0; x < size; x++)
                {
                    alphaArray[i] = new Color(r[i], g[i], b[i], a[i]);
                    i++;
                }
            }            
        }

        ShapeTex.SetPixels(alphaArray);
        ShapeTex.Apply();

        AssetDatabase.CreateAsset(ShapeTex, "Assets/Resources/CloudShape.asset");
        Debug.Log("Saving texture to file");

    }

    public void GenerateDetailTexture(int size)
    {
        DetailTex = new Texture3D(size, size, size, TextureFormat.RGBA32, true);

        Color[] alphaArray = new Color[size * size * size];

        Worley w = new Worley(size, size, size);

        float[] r = w.Sample(2, 6);
        float[] g = w.Sample(3, 6);
        float[] b = w.Sample(5, 6);   
        
        int i = 0;
        for (int z = 0; z < size; z++)
        {
            for (int y = 0; y < size; y++)
            {
                for (int x = 0; x < size; x++)
                {
                    alphaArray[i] = new Color(r[i], g[i], b[i]);
                    i++;
                }
            }
        }

        DetailTex.SetPixels(alphaArray);
        DetailTex.Apply();

        AssetDatabase.CreateAsset(DetailTex, "Assets/Resources/CloudDetail.asset");

    }
    #endregion

}

