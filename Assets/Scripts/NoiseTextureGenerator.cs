﻿using UnityEditor;
using UnityEngine;
using System.IO;

namespace Maelstrom.Noise
{
    public enum TextureType { Texture2D, Texture3D };
    public enum NoiseType { Perlin, Worley, PerlinWorley };

    [ExecuteInEditMode]
    public class NoiseTextureGenerator : MonoBehaviour
    {
        [SerializeField]
        private Material material2D;
        [SerializeField]
        private Material material3D;

        public TextureType texType;
        private TextureType lastType;

        private Texture2D tex2D;
        private Texture3D tex3D;
        public Vector3 textureSize;

        [Header("Red Channel")]
        public NoiseType redNoiseType;
        public int redFrequency;
        public int redOctaves;

        [Header("Green Channel")]
        public NoiseType greenNoiseType;
        public int greenFrequency;
        public int greenOctaves;

        [Header("Blue Channel")]
        public NoiseType blueNoiseType;
        public int blueFrequency;
        public int blueOctaves;

        [Header("Alpha Channel")]
        public NoiseType alphaNoiseType;
        public int alphaFrequency;
        public int alphaOctaves;

        GameObject quad;
        GameObject cube;

        Perlin perlin;
        Worley worley;

        #region Main methods

        void Awake()
        {
            material2D = new Material(Shader.Find("Unlit/Texture"));
            material3D = new Material(Shader.Find("Volume/Standard"));

            MeshRenderer[] meshes = GetComponentsInChildren<MeshRenderer>(true);
            quad = meshes[0].gameObject;
            cube = meshes[1].gameObject;

            meshes[0].material = material2D;
            meshes[1].material = material3D;

            texType = TextureType.Texture2D;
            lastType = texType;

            quad.hideFlags = HideFlags.HideInHierarchy;
            cube.hideFlags = HideFlags.HideInHierarchy;
        }

        void Update()
        {
            if (texType != lastType)
                OnTextureTypeChanged();

        }

        public void OnTextureTypeChanged()
        {
            if (texType == TextureType.Texture2D)
            {
                quad.SetActive(true);
                cube.SetActive(false);

                Camera.main.transform.position = new Vector3(0f, 0f, -10f);
                Camera.main.transform.eulerAngles = Vector3.zero;
            }
            else if (texType == TextureType.Texture3D)
            {
                cube.SetActive(true);
                quad.SetActive(false);

                Camera.main.transform.position = new Vector3(0f, 10f, -10f);
                Camera.main.transform.eulerAngles = new Vector3(45f, 0f, 0f);
            }

            lastType = texType;
        }

        public void Generate2DTexture()
        {
            int sizeX = (int)textureSize.x;
            int sizeY = (int)textureSize.y;

            tex2D = new Texture2D(sizeX, sizeY, TextureFormat.RGBA32, true);

            Color[] alphaArray = new Color[sizeX * sizeY];

            Perlin p = new Perlin(sizeX, sizeY, 1);
            Worley w = new Worley(sizeX, sizeY, 1);

            float[] r, g, b, a;
            GetSamples(p, w, out r, out g, out b, out a);

            int i = 0;
            for (int y = 0; y < sizeY; y++)
            {
                for (int x = 0; x < sizeX; x++)
                {
                    //prevent fully transparent image
                    if (alphaFrequency == 0 && alphaOctaves == 0)
                    {
                        alphaArray[i] = new Color(r[i], g[i], b[i], 1f);
                    }
                    else
                        alphaArray[i] = new Color(r[i], g[i], b[i], a[i]);
                    i++;
                }
            }

            tex2D.SetPixels(alphaArray);
            tex2D.Apply();
            material2D.SetTexture("_MainTex", tex2D);
        }

        public void Generate3DTexture()
        {
            int sizeX = (int)textureSize.x;
            int sizeY = (int)textureSize.y;
            int sizeZ = (int)textureSize.z;

            tex3D = new Texture3D(sizeX, sizeY, sizeZ, TextureFormat.RGBA32, true);

            Color[] alphaArray = new Color[sizeX * sizeY * sizeZ];

            Perlin p = new Perlin(sizeX, sizeY, sizeZ);
            Worley w = new Worley(sizeX, sizeY, sizeZ);

            float[] r, g, b, a;
            GetSamples(p, w, out r, out g, out b, out a);           

            int i = 0;
            for (int z = 0; z < sizeZ; z++)
            {
                for (int y = 0; y < sizeY; y++)
                {
                    for (int x = 0; x < sizeX; x++)
                    {
                        alphaArray[i] = new Color(r[i], g[i], b[i], a[i]);
                        i++;
                    }
                }
            }

            tex3D.SetPixels(alphaArray);
            tex3D.Apply();
            material3D.SetTexture("_MainTex", tex3D);

        }

        void GetSamples(Perlin p, Worley w, out float[] r, out float[] g, out float[] b, out float[] a)
        {
            Debug.Log("Generating Red Channel...");
            if (redNoiseType == NoiseType.Perlin)
                r = p.Sample(redFrequency, redOctaves);
            else if (redNoiseType == NoiseType.Worley)
                r = w.Sample(redFrequency, redOctaves, true);
            else
            {
                float[] r1 = p.Sample(redFrequency, redOctaves);
                float[] r2 = w.Sample(redFrequency, redOctaves, true);

                r = new float[r1.Length];

                for (int i = 0; i < r.Length; i++)
                {
                    r[i] = (r1[i] + r2[i]) / 2f;
                }                
            }

            Debug.Log("Generating Green Channel...");
            if (greenNoiseType == NoiseType.Perlin)
                g = p.Sample(greenFrequency, greenOctaves);
            else if (greenNoiseType == NoiseType.Worley)
                g = w.Sample(greenFrequency, greenOctaves, true);
            else
            {
                float[] g1 = p.Sample(greenFrequency, greenOctaves);
                float[] g2 = w.Sample(greenFrequency, greenOctaves, true);

                g = new float[g1.Length];

                for (int i = 0; i < g1.Length; i++)
                {
                    g[i] = (g1[i] + g2[i]) / 2f;
                }
            }

            Debug.Log("Generating Blue Channel...");
            if (blueNoiseType == NoiseType.Perlin)
                b = p.Sample(blueFrequency, blueOctaves);
            else if (blueNoiseType == NoiseType.Worley)
                b = w.Sample(blueFrequency, blueOctaves, true);
            else
            {
                float[] b1 = p.Sample(blueFrequency, blueOctaves);
                float[] b2 = w.Sample(blueFrequency, blueOctaves, true);

                b = new float[b1.Length];

                for (int i = 0; i < b.Length; i++)
                {
                    b[i] = (b1[i] + b2[i]) / 2f;
                }
            }

            Debug.Log("Generating Alpha Channel...");
            if (alphaNoiseType == NoiseType.Perlin)
                a = p.Sample(alphaFrequency, alphaOctaves);
            else if (alphaNoiseType == NoiseType.Worley)
                a = w.Sample(alphaFrequency, alphaOctaves, true);
            else
            {
                float[] a1 = p.Sample(alphaFrequency, alphaOctaves);
                float[] a2 = w.Sample(alphaFrequency, alphaOctaves, true);

                a = new float[a1.Length];

                for (int i = 0; i < a.Length; i++)
                {
                    a[i] = (a1[i] + a2[i]) / 2f;
                }
            }
        }
        #endregion

        public void SaveTexture(string filename, TextureType type)
        {
            string path;

            if (type == TextureType.Texture2D)
            {
                filename = filename + ".png";
                path = Application.dataPath + "/Resources/";
                
                var bytes = tex2D.EncodeToPNG();
                File.WriteAllBytes(path + filename, bytes);
                Debug.Log("Texture saved to: " + path + filename);

            }
            else if (type == TextureType.Texture3D)
            {
                filename = filename + ".asset";
                path = "Assets/Resources/";

                AssetDatabase.CreateAsset(tex3D, path + filename);
                Debug.Log("Texture saved to: " + path + filename);
            }       
        }
    }
}