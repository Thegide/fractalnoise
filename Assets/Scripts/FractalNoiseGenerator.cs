﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;

public class FractalNoiseGenerator : MonoBehaviour {

    public enum NoiseType { Perlin, Worley };

    public Material material;
    private Texture2D texture;
    public NoiseType noiseType = NoiseType.Perlin;
    
    public int width = 256;
    public int height = 256;
    [Range(1,32)]
    public int frequency = 2;
    [Range(1, 10)]
    public int octaves = 6;
    public bool invert = false;

    //seed generation
    private float offsetX;    
    private float offsetY;
    private float offsetZ;

    #region Main methods
    void Start()
    {
        GenerateTexture();
    }

    public void GenerateTexture()
    {
        //seed generation
        offsetX = UnityEngine.Random.Range(0, width);
        offsetY = UnityEngine.Random.Range(0, height);

        if (noiseType == NoiseType.Perlin)
        {
            material.mainTexture = GeneratePerlinTexture();
        }
        else if (noiseType == NoiseType.Worley)
        {
            material.mainTexture = GenerateWorleyTexture();
        }
    }
    #endregion

    #region Perlin Generation

    Texture2D GeneratePerlinTexture()
    {
        Texture2D texture = new Texture2D(width, height);

        float[,] pixelColors = new float[width, height];

        //Generate a perlin noise map for the nexture
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                pixelColors[x, y] = GetPerlinColor(x, y);
            }
        }

        //Normalize the colors in the array (0..1)
        Normalize(pixelColors);
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                texture.SetPixel(x, y, new Color(pixelColors[x, y], pixelColors[x, y], pixelColors[x, y]));
            }
        }

        texture.Apply();
        return texture;
    }

    //fBm function
    float GetPerlinColor(int x, int y)
    {
        double sum = 0;
        double value = 0;
        int freq = frequency;  //starting frequency
        int px, py;
        px = py = 1;  //period of 1, since coordinates are scaled to 0..1

        //remap coordinates to 0..1
        float xCoord = (x + offsetX) / width;
        float yCoord = (y + offsetY) / height;

        for (int i = 0; i < octaves; i++)
        {
            value = PNoise2(xCoord * freq, yCoord * freq, px * freq, py * freq);            

            sum += value / freq;
            freq *= 2;            
        }

        //not needed as we norrmalize the values anyways...
        //float sample = (float)(sum + 1.0f) * 0.5f;  //perlin values are signed... 

        return (float)sum;
    }
          
    //---------------------------------------------------------------------
    /** 2D float Perlin periodic noise.
     * px, py are the periodicity of the noise
     */
    float PNoise2(float x, float y, int px, int py)
    {
        int ix0, iy0, ix1, iy1;
        float fx0, fy0, fx1, fy1;
        float s, t, nx0, nx1, n0, n1;

        ix0 = Floor(x); // Integer part of x
        iy0 = Floor(y); // Integer part of y
        fx0 = x - ix0;        // Fractional part of x
        fy0 = y - iy0;        // Fractional part of y
        fx1 = fx0 - 1.0f;
        fy1 = fy0 - 1.0f;
        ix1 = ((ix0 + 1) % px) & 0xff;  // Wrap to 0..px-1 and wrap to 0..255
        iy1 = ((iy0 + 1) % py) & 0xff;  // Wrap to 0..py-1 and wrap to 0..255
        ix0 = (ix0 % px) & 0xff;
        iy0 = (iy0 % py) & 0xff;

        t = Fade(fy0);
        s = Fade(fx0);

        nx0 = Grad2(perm[ix0 + perm[iy0]], fx0, fy0);
        nx1 = Grad2(perm[ix0 + perm[iy1]], fx0, fy1);
        n0 = Lerp(nx0, nx1, t);

        nx0 = Grad2(perm[ix1 + perm[iy0]], fx1, fy0);
        nx1 = Grad2(perm[ix1 + perm[iy1]], fx1, fy1);
        n1 = Lerp(nx0, nx1, t);

        return 0.507f * (Lerp(n0, n1, s));
    }
    
    // Fade function as defined by Ken Perlin.  This eases coordinate values
    // so that they will "ease" towards integral values.  This ends up smoothing
    // the final output.
    float Fade(float t)
    {
        return t * t * t * (t * (t * 6 - 15) + 10);   // 6t^5 - 15t^4 + 10t^3
    }

    float Grad2(int hash, float x, float y)
    {
        int h = hash & 7;      // Convert low 3 bits of hash code
        float u = h < 4 ? x : y;  // into 8 simple gradient directions,
        float v = h < 4 ? y : x;  // and compute the dot product with (x,y).
        return (Convert.ToBoolean(h & 1) ? -u : u) + (Convert.ToBoolean(h & 2) ? -2.0f * v : 2.0f * v);
    }

    //Hash table
    int[] perm = new int[] {151,160,137,91,90,15,
    131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,
    190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,
    88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,
    77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,
    102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,
    135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,
    5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,
    223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,
    129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,
    251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,
    49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
    138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180,
    151,160,137,91,90,15,
    131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,
    190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,
    88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,
    77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,
    102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,
    135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,
    5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,
    223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,
    129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,
    251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,
    49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
    138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180
    };
    #endregion

    #region Worley Generation

    Texture2D GenerateWorleyTexture()
    {

        texture = new Texture2D(width, height, TextureFormat.RGBA32, false);

        float[,] pixelColors = new float[width, height];

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                pixelColors[x, y] = GetWorleyColor(x, y);
            }
        }

        //Normalize the colors in the array (0..1)
        Normalize(pixelColors);
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                texture.SetPixel(x, y, new Color(pixelColors[x, y], pixelColors[x, y], pixelColors[x, y]));
            }
        }

        //Debug.Log("cube min: " + cubemin + " cubemax: " + cubemax);
        texture.Apply();
        return texture;
    }

    //fBm function
    float GetWorleyColor(int x, int y, int z = 0)
    {       
        double value = 0;
        double sum = 0;               
        int freq = frequency;
   
        //remap coordinates to 0..1
        float xCoord = (x + offsetX) / width;   
        float yCoord = (y + offsetY) / height;

        for (int i = 0; i < octaves; i++)
        {            
            value = WorleyNoise(xCoord * freq, yCoord * freq, 1f * freq, freq);
            sum += value / freq;
            freq *= 2;
        }

        if (!invert)
            return (float)sum;
        else
            return 1.0f - (float)sum;
    }

    //max_order needs to be < 5;
    //F is feature point distances
    //
    float WorleyNoise(float x, float y, float z, int period)
    {
        //x = x > period ? x - period : x;
        //y = y > period ? y - period : y;
        //z = z > period ? z - period : z;

        /* Determine which cube the evaluation point is in */
        int xi = Floor(x);
        int yi = Floor(y);
        int zi = Floor(z);

        int current_cube_x, current_cube_y, current_cube_z;
        float shortest_distance = 1000.0f;

        /* Loop over the neightbors */
        for (int i = -1; i < 2; i++)
        {
            for (int j = -1; j < 2; j++)
            {
                for (int k = -1; k < 2; k++)
                {

                    current_cube_x = xi + i;
                    current_cube_y = yi + j;
                    current_cube_z = zi + k;

                    int hash_code = Hash(Mod(current_cube_x, period), Mod(current_cube_y, period), Mod(current_cube_z, period));

                    ///* Determine how many feature points this cube has (1-2) */
                    int points = hash_code % 2 + 1;

                    ///* Look for the shortest distance from evaluation point to feature point */
                    for (int p = 2; p <= points + 1; p++) {
                    
                        float feature_x = (float)current_cube_x + (float)(Mod(p * hash_code * 5783, 256)) / 256.0f;
                        float feature_y = (float)current_cube_y + (float)(Mod(p * hash_code * 9419, 256)) / 256.0f;
                        float feature_z = (float)current_cube_z + (float)(Mod(p * hash_code * 2753, 256)) / 256.0f;

                        float distance = Mathf.Sqrt((feature_x - x) * (feature_x - x) + (feature_y - y) * (feature_y - y) + (feature_z - z) * (feature_z - z));
                        if (distance < shortest_distance)
                        {
                            shortest_distance = distance;
                        }
                    }
                }
            }
        }

        /* The inverse of the shortest distance normalized with the theoretical maximum */
        float inv = shortest_distance; // 1.0f - shortest_distance;

        if (inv < 0.0)
        {
            inv = 0.0f;
        }

        /* Change from 0 - 1 to -1 - 1*/
        return inv * 2.0f - 1.0f;
    }

    int Mod(int a, int b)
    {
        int r = a % b;
        return r < 0 ? r + b : r;
    }

    int Hash(int x, int y, int z)
    {
        return (int)((Mathf.Sin((x + 13) * (y + 57) * (z + 89) * 11351.0f) + 1.0) * 0.5 * 256);
    }

    #endregion


    #region Helper Functions

    //faster than Mathf.FloorToInt();
    int Floor(float x)
    {
        return ((x > 0) ? (int)x : (int)x - 1);
    }

    float Lerp(float a, float b, float t)
    {
        return a + t * (b - a);
    }

    void Normalize(float[,] pixels)
    {
        float max = Mathf.NegativeInfinity;
        float min = Mathf.Infinity;

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (pixels[x, y] < min)
                {
                    min = pixels[x, y];
                }
                if (pixels[x, y] > max)
                {
                    max = pixels[x, y];
                }
            }
        }

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                float rawValue = pixels[x, y];
                pixels[x, y] = (rawValue - min) / (max - min);
            }
        }
    }

    #endregion

    #region IO
    public void SaveToPNG()
    {
        if (noiseType == NoiseType.Perlin)
        {
            SaveTextureToFile(material.mainTexture as Texture2D, "Perlin.png");
        }
        else if (noiseType == NoiseType.Worley)
        {
            SaveTextureToFile(material.mainTexture as Texture2D, "Worley.png");
        }
        
    }

    void SaveTextureToFile(Texture2D texture, string fileName)
    {
        var bytes = texture.EncodeToPNG();

        File.WriteAllBytes(Application.dataPath + "/" + fileName, bytes);

        Debug.Log("Saved to: " + Application.dataPath + "/" + fileName);
    }
    #endregion
}
