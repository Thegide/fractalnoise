﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;

public class FractalNoiseGenerator3D : MonoBehaviour {

    public enum NoiseType { Perlin, Worley };

    public Material material;
    private Texture3D texture;
    public NoiseType noiseType = NoiseType.Perlin;
    
    public int width = 128;
    public int height = 128;
    public int depth = 128;

    public Perlin perlin;
    public Worley worley;

    #region Main methods
    void Start()
    {
        perlin = new Perlin(width, height, depth);
        worley = new Worley(width, height, depth);
        GenerateTexture();
    }

    public void GenerateTexture()
    {
        if (noiseType == NoiseType.Perlin)
        {            
            perlin.Generate();
            material.mainTexture = perlin.Texture;
        }
        else if (noiseType == NoiseType.Worley)
        {
            worley.Generate();
            material.mainTexture = worley.Texture;
        }
    }
    #endregion

    #region IO
    public void SaveToPNG()
    {
        if (noiseType == NoiseType.Perlin)
        {
            SaveTextureToFile(material.mainTexture as Texture3D, "Perlin.png");
        }
        else if (noiseType == NoiseType.Worley)
        {
            SaveTextureToFile(material.mainTexture as Texture3D, "Worley.png");
        }
        
    }

    void SaveTextureToFile(Texture3D texture, string fileName)
    {
    //    var bytes = texture.EncodeToPNG();

    //    File.WriteAllBytes(Application.dataPath + "/" + fileName, bytes);

    //    Debug.Log("Saved to: " + Application.dataPath + "/" + fileName);
    }
    #endregion
}
