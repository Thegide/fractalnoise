﻿using UnityEngine;
using UnityEditor;

namespace Maelstrom.Noise
{
    [CustomEditor(typeof(NoiseTextureGenerator))]
    public class EditorScript : Editor
    {
        string filename = "";

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            NoiseTextureGenerator gen = (NoiseTextureGenerator)target;

            if (GUILayout.Button("Generate Texture"))
            {
                if (gen.texType == TextureType.Texture2D)
                {
                    gen.Generate2DTexture();
                }
                else if (gen.texType == TextureType.Texture3D)
                {
                    gen.Generate3DTexture();
                }
            }

            filename = EditorGUILayout.TextField("File Name: ", filename);
            EditorGUILayout.HelpBox("Files are saved to ../Assets/Resources", MessageType.Info);

            if (GUILayout.Button("Save Texture"))
            {
                Debug.Log("Saving texture to file");
                gen.SaveTexture(filename, gen.texType);
            }
        }
    }
}