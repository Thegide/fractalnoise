﻿using System;
using UnityEngine;

[Serializable]
public sealed class Worley {

    //texture dimensions
    private int width = 128;
    private int height = 128;
    private int depth = 128;

    //pseudorandom texture generation
    private float offsetX;
    private float offsetY;
    private float offsetZ;

    private Texture3D _texture;
    public Texture3D Texture { get { return _texture; } }

    [Header("Worley")]
    [Range(1, 32)]
    public int frequency = 3;
    [Range(1, 10)]
    public int octaves = 1;
    public bool invert = false;

    public Worley(int width, int height, int depth)
    {
        this.width = width;
        this.height = height;
        this.depth = depth;
    }

    public void Generate()
    {
        offsetX = UnityEngine.Random.Range(0, width);
        offsetY = UnityEngine.Random.Range(0, height);
        offsetZ = UnityEngine.Random.Range(0, depth);

        _texture = new Texture3D(width, height, depth, TextureFormat.RGBA32, true);

        float[] sampleArray = new float[width * height * depth];
        int i = 0;
        for (int z = 0; z < depth; z++)
        {
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    sampleArray[i] = WorleySampler(x, y, z, frequency, octaves, invert);
                    i++;
                }
            }
        }

        Normalize(sampleArray);

        Color[] colorArray = new Color[width * height * depth];
        i = 0;
        for (int z = 0; z < depth; z++)
        {
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    float val = sampleArray[i];
                    colorArray[i] = new Color(val, val, val);
                    i++;
                }
            }
        }

        _texture.SetPixels(colorArray);
        _texture.Apply();
    }

    public float[] Sample(int frequency, int octaves, bool invert = true)
    {
        offsetX = UnityEngine.Random.Range(0, width);
        offsetY = UnityEngine.Random.Range(0, height);
        offsetZ = UnityEngine.Random.Range(0, depth);

        this.frequency = frequency;
        this.octaves = octaves;

        float[] sampleArray = new float[width * height * depth];
        int i = 0;
        for (int z = 0; z < depth; z++)
        {
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    sampleArray[i] = WorleySampler(x, y, z, frequency, octaves, invert);
                    i++;
                }
            }
        }

        Normalize(sampleArray);
        return sampleArray;
    }

    float WorleySampler(int x, int y, int z, int freq, int octaves, bool invert)
    {
        double value = 0;
        double sum = 0;

        float xCoord = (x + offsetX) / width;
        float yCoord = (y + offsetY) / height;
        float zCoord = (z + offsetZ) / depth;

        for (int i = 0; i < octaves; i++)
        {
            value = WorleyNoise(xCoord * freq, yCoord * freq, zCoord * freq, freq);
            sum += value / freq;
            freq *= 2;
        }

        if (invert)
            return 1.0f - (float)sum;
        else
            return (float)sum;
    }

    float WorleyNoise(float x, float y, float z, int period)
    {
        /* Determine which cube the evaluation point is in */
        int xi = Floor(x);
        int yi = Floor(y);
        int zi = Floor(z);

        int current_cube_x, current_cube_y, current_cube_z;
        float shortest_distance = 1000.0f;

        /* Loop over the neightbors */
        for (int i = -1; i < 2; i++)
        {
            for (int j = -1; j < 2; j++)
            {
                for (int k = -1; k < 2; k++)
                {

                    current_cube_x = xi + i;
                    current_cube_y = yi + j;
                    current_cube_z = zi + k;

                    int hash_code = Hash(Mod(current_cube_x, period), Mod(current_cube_y, period), Mod(current_cube_z, period));

                    ///* Determine how many feature points this cube has (1-2) */
                    int points = hash_code % 2 + 1;

                    ///* Look for the shortest distance from evaluation point to feature point */
                    for (int p = 2; p <= points + 1; p++)
                    {

                        float feature_x = (float)current_cube_x + (float)(Mod(p * hash_code * 5783, 256)) / 256.0f;
                        float feature_y = (float)current_cube_y + (float)(Mod(p * hash_code * 9419, 256)) / 256.0f;
                        float feature_z = (float)current_cube_z + (float)(Mod(p * hash_code * 2753, 256)) / 256.0f;

                        float distance = Mathf.Sqrt((feature_x - x) * (feature_x - x) + (feature_y - y) * (feature_y - y) + (feature_z - z) * (feature_z - z));
                        if (distance < shortest_distance)
                        {
                            shortest_distance = distance;
                        }
                    }
                }
            }
        }

        if (shortest_distance < 0.0)
        {
            shortest_distance = 0.0f;
        }

        /* Change from 0 - 1 to -1 - 1*/
        return shortest_distance * 2.0f - 1.0f;
    }

    int Hash(int x, int y, int z)
    {
        return (int)((Mathf.Sin((x + 13) * (y + 57) * (z + 89) * 11351.0f) + 1.0) * 0.5 * 256);
    }

    void Normalize(float[] samples)
    {
        float max = Mathf.NegativeInfinity;
        float min = Mathf.Infinity;

        for (int i = 0; i < samples.Length; i++)
        {
            if (samples[i] < min) { min = samples[i]; }
            if (samples[i] > max) { max = samples[i]; }
        }

        for (int i = 0; i < samples.Length; i++)
        {
            float raw = samples[i];
            samples[i] = (raw - min) / (max - min);
        }
    }

    //faster than Mathf.FloorToInt();
    int Floor(float x)
    {
        return ((x >= 0) ? (int)x : (int)x - 1);
    }

    int Mod(int a, int b)
    {
        int r = a % b;
        return r < 0 ? r + b : r;
    }
}
