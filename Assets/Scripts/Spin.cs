﻿using System.Collections;
using UnityEngine;

public class Spin : MonoBehaviour {

    public float speed = 1f;

	void Update () {
        transform.Rotate(Vector3.up, speed * Time.deltaTime);
	}
}
